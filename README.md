# devops

Здесь хранятся настройки пайплайнов.
* ci-deploy-stages.yml - стэйджи пайплайна и варианты деплоя в разные среды
* ci-ansible-deploy.yml - деплой проекта через ансибл
* ci-docker-build.yml - билд проекта через докер
* ci-sonarqube-tests.yml - тесты на уязвимости и качество кода
